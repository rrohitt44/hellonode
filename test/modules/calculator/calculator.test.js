const calculator = require('../../../modules/calculator/calculator');
const assert = require('assert').strict;

describe("Unit testing Calculator module", function(){
	it("test add and should return addition of two numbers", function() {
		assert.notStrictEqual(0, 1);
	});
});

describe('Simple Math Test', () => {
 it('should return 2', () => {
        assert.equal(2, calculator.add(1,1));
    });
 it('should return 9', () => {
        assert.equal(3 * 3, 9);
    });
});
